/*
//  State.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti

The State class represents any block configuration on a game board. The simplest 
way to represent a state of this game is a matrix of integers, where zero stands 
for blank and integers for block labels. A graphical example of a state is below

                            | 0 | 0 | 6 |
                            | 0 | 1 | 4 |
                            | 5 | 3 | 2 |
*/

#ifndef State_h
#define State_h


#include <list>
#include <iostream>
#include <stdlib.h>
#include <vector>

using namespace std;

const int BOARD_SIZE = 3;

class State {
private:
    int numSize = (BOARD_SIZE * BOARD_SIZE) - BOARD_SIZE;
    // int **boardP; // Experimental Board
  
public:
    int board[BOARD_SIZE][BOARD_SIZE];
    /* Should be implemented as private but ran out of time
     */
    
    State();
    /* Initializes a random game board with numbers
     * from (n*n)-n, where if n = 3 the numbers, 1 - 6
     * would be added. The constructor ensures only n 0's
     * are added and no numbers appear above a zero.
     */
    
    State(const State &toCopy);
    /* Initializes a game state from an existing state
     * Object.
     */
    
    void print();
    /* prints the this board to the screen
     * 2 |0|0|2|
     * 1 |1|0|3|
     * 0 |6|5|4|
     *    0 1 2
     */
    
    State copy();
    /* Returns a copy of the current board
     */
    
    
    void operator = (const State &sToCopy);
    /* Overloads the assignment operator for a Board type
     *
     * @param sToCopy - The board to be copied
     */
    
    bool operator==(State &check) const;
    /* Overloads the equals too operator
     * this allows a check if one board is equal to another.
     *
     * @param check - the board to be cheked if it equals this board
     * @return bool - True if each position on the board holds the same value
     *                False if a value is different
     */
    
    int getVal(int row, int col);
    /* returns the value store at a specified position
     *
     * @param row - the row the value is in.
     * @param col - the column the value is in.
     * @return int - the value stored at that row and column
     */
    
    int getBoardSize();
    /* Returns the value of n, the BOARD_SIZE constant
     *
     * @return int - the value of BOARD_SIZE
     */
};

/* Default constructor */
State::State() {
    list<int> numberAdded;
    int zeroCount = 0;
    int randNo;
    
    
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0 ; col < BOARD_SIZE; col++) {
            
            // Set current size of numbers added
            int sizeBeforeAdd = (int) numberAdded.size();
            
            // Add a zero ontop of other zeros or if all numbers added.
            if ((row != 0 && board[row - 1][col] == 0) || (numberAdded.size() > numSize)) {
                board[row][col] = 0;
                zeroCount++;
            } else {
                // Seed random with time
                srand((int) time(NULL));
                
                // while a new number hasn't been added
                while (numberAdded.size() <= sizeBeforeAdd){
                    
                    // gen random no. with zeros or without
                    if (zeroCount < BOARD_SIZE) {
                        randNo = (rand() % (numSize + 1));
                    } else {
                        randNo = (rand() % numSize) + 1;
                    }
                    
                    // check it is unique by deleting then adding
                    numberAdded.remove(randNo);
                    numberAdded.push_front(randNo);
                }
                
                // add new number to the board
                board[row][col] = numberAdded.front();
            }
        }
    }
}

/* Copy constructor */
State::State(const State &toCopy) {
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            this->board[row][col] = toCopy.board[row][col];
        }
    }
}

/* Print */
void State::print() {
    for (int i = BOARD_SIZE - 1 ; i >= 0; i--) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            cout << "|" << board[i][j];
        }
        
        cout << "|" << endl;
    }
    cout << " - - - " << endl;
}

/* Copy */
State State::copy() {
    State toReturn(*this);
    
    return toReturn;
}

/* Assignment overload */
void State::operator = (const State &sToCopy) {
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            board[row][col] = sToCopy.board[row][col];
        }
    }
}

/* Equals too overload */
bool State::operator==(State &check) const {
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            if (board[row][col] != check.getVal(row, col)) {
                return false;
            }
        }
    }
    
    return true;
}

/* Get Value */
int State::getVal(int row, int col) {
    return board[row][col];
}

/* Get BOARD_SIZE */
int State::getBoardSize() {
    return BOARD_SIZE;
}

#endif /* State_h */