//
//  UserInterface.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti
// 18577857

#ifndef UserInterface_h
#define UserInterface_h

#include <iostream>
#include <string>
#include "State.h"
#include "Goal.h"
#include "Plan.h"

class UserInt {
private:
    string goalTypes[3] = {"Atom Goal", "Disjunctive Goal", "Conjunctive Goal"};
public:
    UserInt();
    /* Initialises a random state then collects user input
     * for the type of gola they wish to achieve,
     * after which initialises the plan to solve the goal.
     */
    
    Goal getGoal();
    /* Obtains user input for an atom goal
     * 
     * @return Goal - the user defined atom goal
     */
    
    int getNum(string valType, int minVal, int maxVal);
    /* Collects a number of the user between the defined
     * parameter minVal and maxVal inclusive.
     *
     * @param valType - A string to specify the type of input in text i.e "row"
     * @param minVal - the minimum accepted value
     * @param maxVal - the maximum accepted value
     */
    
    int selectGoalType();
    /* Allows the user to select what type of goal they wish to achieve
     *
     * @return int - a number between 1 - 3 representing the goal type
     */
    
    vector<Goal> getDisGoal(string goalType);
    /* gets a collection of goals from the user,
     * this can be used for conjunctive and disjunctive goal collection
     *
     * @param goalType - a string to represent teh goal type to collect i.e. "Conjunctive"
     * @return vector<Goal> - returns a collection of user enterd goals
     */
};

/* Constructor */
UserInt::UserInt() {
    
    // Create initail random state
    State initial;
    cout << "------- Initial State -------" << endl;
    initial.print();
    cout << "-----------------------------" << endl;
    
    // Get user defined goal choice
    int choice = selectGoalType();
    
    switch (choice) {
        case 1:
        {
            // Set up Atom goal
            Goal atomGoal = getGoal();
            Plan takePlanA(initial, atomGoal);
            break;
        }
        case 2:
        {
            // Set up Disjunctive goal
            Disjunctive disjuncGoal(getDisGoal("Disjunctive"));
            Plan takePlanD(initial, disjuncGoal);
            break;
        }
        case 3:
        {
            // set up conjunctive goal
            Conjunct conjuncGoal(getDisGoal("Conjunctive"));
            
            // ensure conjunctive goal is possible
            if (conjuncGoal.checkIfStateExists()) {
                Plan takePlanC(initial, conjuncGoal);
            } else {
                cout << "The Goals you have entered are not achievable" << endl;
            }
        }
    }
}

/* Get Goal */
Goal UserInt::getGoal() {
    // Get number , row and column for a goal
    cout << "Goals are structured as [tile, row, column]" << endl;
    cout << endl;
    int num = getNum("tile", 1, 6);
    int row = getNum("row", 0, 2);
    int col = getNum("column", 0, 2);
    
    Goal aGoal(num, row, col);
    
    return aGoal;
}

/* get Number */
int UserInt::getNum(string valType, int minVal, int maxVal) {
    int num;
    
    // Get a number between and inclusive of the specified max and min values
    do {
        cout << "Please enter the " << valType << " for the goal [" << minVal << " - " << maxVal << "]" << endl;
        cin >> num;
        
        if (num < minVal || num > maxVal) {
            cout << "ERROR: The " << valType << " entered must be between " << minVal << " - " << maxVal << endl;
        }
    } while (num < minVal || num > maxVal);
    
    return num;
}

/* Select Goal Type */
int UserInt::selectGoalType() {
    cout << "Please select a goal type: [1- 3] " << endl;
    for (int i = 0; i < 3; i++) {
        cout << i+1 << ". " << goalTypes[i] << endl;
    }
    
    int choice;
    do {
        cin >> choice;
        
        if (choice > 3 || choice < 1) {
            cout << "ERROR: the choice must be between 1 - 3" << endl;
        }
    } while (choice > 3 || choice < 1);
    return choice;
}

/*Dijunctive Goal */
vector<Goal> UserInt::getDisGoal(string goalType) {
    vector<Goal> goals;
    char choice = 'y';
    
    // Continue to collect goals until user selects to stop "n"
    do {
        cout << "Add a goal to your " << goalType << " goal" << endl;
        Goal newGoal = getGoal();
        goals.push_back(newGoal);
        cout << "would you like to add another goal? [y/n]" << endl;
        cin >> choice;
        
    } while(choice != 'n');
    return goals;
}

#endif /* UserInterface_h */
