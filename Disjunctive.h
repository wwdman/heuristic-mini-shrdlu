//
//  Disjunctive.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti
// 18577857

#ifndef Disjunctive_h
#define Disjunctive_h

#include <vector>
#include "Goal.h"

class Disjunctive:public Goal {
private:
    vector<Goal> disGoals;
    
public:
    Disjunctive(){}
    /* Default constructor
     */
    
    Disjunctive(vector<Goal> goals);
    /* Constructor from a vector of goals
     */
    
    vector<Goal> * getDisGoal();
    /* get the disjunctive vector of goals
     */
    
    void addDisGoal(int goal, int row, int col);
    /* add a goal to the vector
     */
    
};

/* Constructor */
Disjunctive::Disjunctive(vector<Goal> goals) {
    for (int i = 0; i < goals.size(); i++) {
        disGoals.push_back(goals[i]);
    }
}

/* Get Disjunctive goals */
vector<Goal> * Disjunctive::getDisGoal() {
    return &disGoals;
}

/* Add a goal */
void Disjunctive::addDisGoal(int goal, int row, int col) {
    Goal newGoal(goal, row, col);
    disGoals.push_back(newGoal);
}

#endif /* Disjunctive_h */
