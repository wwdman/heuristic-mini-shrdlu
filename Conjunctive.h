//
//  Conjunctive.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti
// 18577857

#ifndef Conjunctive_h
#define Conjunctive_h

#include <queue>

#include "State.h"
#include "Disjunctive.h"

class Conjunct:public Disjunctive {
    priority_queue<Goal> conGoal;
    
public:
    Conjunct(){}
    /* Default Constructor
     */
    
    Conjunct(vector<Goal> goals);
    /* Constructor created from a vector of goals
     *
     * @param goals - the vector of goals to initialise this conjunctive goal
     */
    
    bool checkIfStateExists();
    /* Checks if a set of conjunctive goals can produce
     * a possible state
     */
    
    priority_queue<Goal> * getConGoal();
    /* Returns a pointer to this priority queue of conjunctive goals
     */
    
};

/* Constructor */
Conjunct::Conjunct(vector<Goal> goals) {
    for (Goal &goal : goals) {
        conGoal.push(goal);
        getDisGoal()->push_back(goal);
    }
    
}

/* Check If state exists (is possible) */
bool Conjunct::checkIfStateExists() {
    int colAndXs[getDisGoal()->size()][2]; // Store the column and the amount of random numbers benath it
    int visited[getDisGoal()->size()]; // Store the visited columns
    bool exists = true;
    int iter = 0;
    int totalXs = 6 - (int) getDisGoal()->size();
    
    // Check each goal, no tile or position of any goal can be the same as another
    // Otherwise the gola is invalid
    for (Goal & goalToCheck : *getDisGoal()) {
        int sameGoal = 0;
        
        colAndXs[iter][0] = goalToCheck.getCol();
        colAndXs[iter][1] = goalToCheck.getRow();
        iter++;
        //cout << "in For 1" << endl;
        
        for (Goal & goal: *getDisGoal()) {
            //cout << "in For 2" << endl;
            
            if (goal == goalToCheck && sameGoal <= 1) {
                sameGoal++;
            } else if (sameGoal > 1) {
                return false;
            } else {
                //Conjunctive goals are not possible
                // If numbers are the same
                if (goal.getNumber() == goalToCheck.getNumber()) {
                    exists = false;
                }
                
                // If positition is the sam
                if (goal.getRow() == goalToCheck.getRow() && goal.getCol() == goalToCheck.getCol()) {
                    exists = false;
                }
            }
        }
    }
    
    // Check the amount of random numbers to be beneath the goal is not greater tha allowed
        // Formula :
    // 6 - (number of goals) - (number of numbers beneath goal) + (number of numbers beneath goal if goals are in same column)
    for (int i = 0; i < getDisGoal()->size(); i++) {
        int col = colAndXs[i][0];
        bool seen = false;
        
        // Check if seen this col, if two goals are in the same column
        for (int j = 0; j < getDisGoal()->size(); j++) {
            
            if (col == visited[j]) {
                seen = true;
            }
        }
        visited[i] = col;
        
        if (seen) {
            if (colAndXs[i][1] == 0) {
                totalXs += 1;
            } else {
                totalXs += colAndXs[i][1];
            }
        } else {
            totalXs -= colAndXs[i][1];
        }
        seen = false;
        
    }
    
    if (totalXs < 0) {
        exists = false;
    }
    
    
    return exists;
}

/* Get Conjunctive Goals */
priority_queue<Goal> * Conjunct::getConGoal() {
    return &conGoal;
}
#endif /* Conjunctive_h */
