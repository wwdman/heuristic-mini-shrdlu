/*
//  Action.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti

The action class represents moving a block from one column to another column 
within a state (@see: State.h). Pair of integers are used (source, destination), 
to represent an action. For instance, (2,1) means moving the highest verticle 
block from Column 2 to Column 1. An action is considered legal in a state only 
if the destination column contains empty slots. In the below example there are 
the following legal actions: (0,1), (1, 0), (2, 0) and (2,1).

                            | 0 | 0 | 6 |
                            | 0 | 1 | 4 |
                            | 5 | 3 | 2 |

Once a legal action is applied to a state, the state will transition to its next 
state. For instance, if we apply the action (2,1) to the above-shown state, its 
next state will be:

                            | 0 | 6 | 0 |
                            | 0 | 1 | 4 |
                            | 5 | 3 | 2 |
*/

#ifndef Action_h
#define Action_h


class Action{
private:
    int sourceCol;
    int destCol;
    int sourceRow;
    int destRow;
    int heuristic;
    
public:
    Action(){}
    /* Default Constructor
     */
    
    Action(int start, int finish, int huerist);
    /* Initialises Action object
     *
     * @param start - is the integer representing the source column
     * @param finish - is the integer representing the destination column
     */
    
    Action(int startRow, int startCol, int finishRow, int finishCol, int heu);
    /* Constructor to make an action with source co-ordinates and destination co-ordinates
     *
     * @param startRow - the source row co-ordinate
     * @param startCol - the source column co-ordinate
     * @param finishRow - the destination row co-ordinate
     * @param finishCol - the destination column co-ordinate
     */
    
    void setSourceDest(int start, int finish);
    /* Allows the private data member source and dest to be set
     *
     * @param start - is the integer representing the source column
     * @param finish - is the integer representing the destination column
     */
    
    void setHeuristic(int heurist);
    /* Sets the actions heuristic value
     *
     * @param heurist - the heuristic value of this action
     */
    
    int getSource();
    /* Method to get the value of the source column of the action
     *
     * @returns the integer representing the source column
     */
    int getDest();
    /* Method to get the value of the destination column of the action
     *
     * @returns the integer representing the destination column
     */
    
    int getHeu() const;
    /* Method to veiw what the heuristic value of this action is
     *
     * @return - the heuristic value [0 - 100]
     */
    
    int getSourRow() const;
    /* Gets the vlaue of the source row
     */
    
    int getSourCol() const;
    /* Gets the value of the source column
     */
    
    int getDestRow() const;
    /* Gets the value of teh destination row
     */
    
    int getDestCol() const;
    /* Gets the value of the destination column
     */
    
    bool operator<(const Action aToCompare) const;
    /* Overrides the "<" operator to compare Actions
     *
     * @return - returns true if aToCompare is greater | false if "this" action  is greater.
     */
    
    void print();
    /* Prints the action to screen
     *
     * S(Row, Col) - D(Row, Col) - Heu: XXX
     */
    
    void operator=(const Action aToCopy);
    /* Overloads the Assignment operator
     */
};

/* Constructor */
Action::Action(int start, int finish, int heurist):sourceCol(start), destCol(finish), heuristic(heurist){}

/* Constructor */
Action::Action(int startRow, int startCol, int finishRow, int finishCol, int heu): sourceRow(startRow), sourceCol(startCol), destRow(finishRow), destCol(finishCol), heuristic(heu){}

/* Set source and Destination columns (Legacy)*/
void Action::setSourceDest(int start, int finish){
    this->sourceCol = start;
    this->destCol = finish;
}

/* Set heuristic value */
void Action::setHeuristic(int heurist) {
    this->heuristic = heurist;
}

/* get source column (Legacy)*/
int Action::getSource() {
    return this->sourceCol;
}

/* Get destination column (Legacy)*/
int Action::getDest() {
    return this->destCol;
}

/* Get heuristic value */
int Action::getHeu() const {
    return this->heuristic;
}

/* Get source row */
int Action::getSourRow() const {
    return this->sourceRow;
}

/* Get source column */
int Action::getSourCol() const {
    return this->sourceCol;
}

/* Gest destination Row */
int Action::getDestRow() const {
    return this->destRow;
}

/* Get Destination column */
int Action::getDestCol() const {
    return this->destCol;
}

/* Less than overload */
bool Action::operator<(const Action aToCompare) const {
    if (this->heuristic < aToCompare.heuristic) {
        return true;
    } else {
        return false;
    }
}

/* Assignment overload */
void Action::operator=(const Action aToCopy) {
    this->sourceRow = aToCopy.getSourRow();
    this->destRow = aToCopy.getDestRow();
    this->sourceCol = aToCopy.getSourCol();
    this->destCol = aToCopy.getDestCol();
    this->heuristic = aToCopy.getHeu();
}

/* Print */
void Action::print() {
    cout << "S(" << getSourRow() << ", " << getSourCol() << ") - D(" << getDestRow() << ", " << getDestCol() << ") - Heu: " << getHeu() << endl;
}
#endif /* Action_h */
