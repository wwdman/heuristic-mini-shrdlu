//
//  LegalAction.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti
// 18577857

#ifndef LegalAction_h
#define LegalAction_h


#include <vector>
#include <queue>
#include "State.h"
#include "Action.h"
#include "Goal.h"

// LegalAction is a bad name for this class, but I did not have time to change it
// It extends the functionality of a State.

class LegalAction:public State {
private:
    priority_queue<Action> legalMoves;
    vector<Action> legalMoves1;
    Goal currentGoal;
    int tileLoc[2];
    
public:
    LegalAction(Goal toAchieve);
    /* Initializes the Action object and
     * Initializes a new current Board then
     * searches it for legal moves.
     */
    
    LegalAction(State cur, Goal toAchieve);
    /* Initializes the Action object and
     * assigns the current Board then
     * searches it for legal moves.
     */
    
    vector<Action> getLegalMoves1();
    /* A method to return the private data member legal moves
     *
     * @retrurn - legalMoves
     */
    
    priority_queue<Action> * getLegalMoves();
    /* A method to return the private data member legal moves
     *
     * @retrurn - a pointer to legalMoves
     */
    
    void search();
    /* Searches the current Board for legal moves.
     * Move is legal if it moves a number to an
     * empty slot (0) and does not exceed Board
     * boundaries.
     *
     * @return A pointer to the legal moves
     */
    
    void findTile();
    /* Finds the location of the current goal tile
     */
    
    State move(State s, Action moves);
    /* Moves one tile from a source position to a destination
     *
     * @param s - the board to preform the move on
     * @param moves - the Action object holding the source
     and destination co-ordinates
     * @return - returns a board with the resulting move
     */
    
    LegalAction copy();
    /* Returns a copy of the this State object
     *
     * @return - a copy of this state
     */
    
    void operator=(const LegalAction &newLegal);
    /* Overloads the assignment operator for a State object
     */
    
    void printMoves();
    /* Prints to screen a list of the current legal moves from this state
     */
    
    int getHeuristic(int safeFromRow, int safeFromCol, int safeToRow, int safeToCol);
    /* Based off an actions source and destination co-ordinates
     * and the current goal tile location, a heuristic value is
     * assigned to that action
     *
     * @param safeFromRow - the source row co-ordinate
     * @param safeFromCol - the source column co-ordinate
     * @param safeToRow - the destination row co-ordinate
     * @param safeToCol - the destination column co-ordinate
     * @return int - a number representing teh actions heuristic value.
     */
};

/* Default Constructor */
LegalAction::LegalAction(Goal toAchieve):currentGoal(toAchieve) {
    findTile();
    search();
}

/* Copy constructor */
LegalAction::LegalAction(State curr, Goal toAchieve):currentGoal(toAchieve) {
    findTile();
    
    for (int row = 0; row < BOARD_SIZE; row++) {
        for (int col = 0; col < BOARD_SIZE; col++) {
            this->board[row][col] = curr.getVal(row, col);
        }
    }
    search();
}

/* Get legal moves (Vector) */
vector<Action> LegalAction::getLegalMoves1() {
    return legalMoves1;
}

/* Get legal Moves */
priority_queue<Action> * LegalAction::getLegalMoves() {
    return &legalMoves;
}

/* Search for legal actions */
void LegalAction::search() {
    list<int *> destCo;
    list<int *> sourceCo;
    
    // Find safe columns
    for (int col = 0; col < getBoardSize(); col++) {
        int row = getBoardSize() - 1;
        
        
        // find
        while (this->getVal(row, col) == 0 && row >= 0) {
            if (row != 0 && this->getVal(row - 1, col) == 0) {
                row--;
            } else {
                destCo.push_front(new int[2]);
                destCo.front()[0] = row;
                destCo.front()[1] = col;
                row = -1;
            }
        }
        
        row = 0;
        
        while (this->getVal(row, col) != 0 && row <= 2) {
            if (row != 2 && this->getVal(row + 1, col) != 0) {
                row++;
            } else {
                sourceCo.push_front(new int[2]);
                sourceCo.front()[0] = row;
                sourceCo.front()[1] = col;
                row = 3;
            }
        }
    }
    
    // Assign safe moves
    int heu = 0;
    for (int* safeTo : destCo) {
        for (int* safeFrom : sourceCo) {
            if (safeFrom[1] != safeTo[1]) {
                
                heu = getHeuristic(safeFrom[0], safeFrom[1], safeTo[0], safeTo[1]);
                // Assign hue
                
                Action legal(safeFrom[0], safeFrom[1], safeTo[0], safeTo[1], heu);
                
                legalMoves.push(legal);
                legalMoves1.push_back(legal);
                legal.print();
            }
        }
    }
    
}

/* Find Tile */
void LegalAction::findTile() {
    for (int row = 0; row < getBoardSize(); row++) {
        for (int col = 0; col < getBoardSize(); col++) {
            if (board[row][col] == currentGoal.getNumber()) {
                tileLoc[0] = row;
                tileLoc[1] = col;
                return;
            }
        }
    }
    
}

/* Move */
State LegalAction::move(State s, Action moves) {
    int row = getBoardSize() -1;
    int temp = 0;
    
    // Swap out col from
    while (temp == 0 && row >= 0) {
        if (s.getVal(row, moves.getSource()) != 0) {
            temp = s.getVal(row, moves.getSource());
            s.board[row][moves.getSource()] = 0;
        } else {
            row--;
        }
    }
    
    row = getBoardSize() -1;
    
    // Swap out col in
    while (temp != 0 && row >= 0) {
        
        if (s.getVal(row-1, moves.getDest()) == 0 && row != 0) {
            row--;
        } else {
            s.board[row][moves.getDest()] = temp;
            temp = 0;
        }
    }
    
    //s.print();
    return s;
}

/* Copy this */
LegalAction LegalAction::copy() {
    LegalAction toReturn(*this);
    
    return toReturn;
}

/* Assignment overload */
void LegalAction::operator = (const LegalAction &newLegal) {
    this->legalMoves = newLegal.legalMoves;
    this->legalMoves1 = newLegal.legalMoves1;
    
}

/* Print legal moves */
void LegalAction::printMoves() {
    for (int i = 0; i < sizeof(legalMoves1); i++) {
        cout << legalMoves1[i].getSource() << ", " << legalMoves1[i].getDest() << endl;
    }
}

/* Get Heuristic value */
int LegalAction::getHeuristic(int safeFromRow, int safeFromCol, int safeToRow, int safeToCol) {
    
    // -------------- check it works for building up to a position -----------//
    
    int heu = 0;
    // Assign hue
    findTile();
    if (tileLoc[0] != 2) {
        if (getVal(tileLoc[0] + 1, tileLoc[1]) != 0) {
            // get stuff off goal tile
            
            // A move from the goal tile column
            // heu 50
            
            if (safeFromCol == tileLoc[1] && safeToCol != currentGoal.getCol()) {
                heu = 50;
            } else if (safeFromCol == tileLoc[1]) {
                heu = 45;
            }
        } else {
            // move goal tile to row 2 position
            // if goal is in row 2 put goal tile in goal position
            // heu 70
            if (currentGoal.getRow() == 2) {
                if ((safeFromRow == tileLoc[0] && safeFromCol == tileLoc[1]) && (safeToRow == currentGoal.getRow() && safeToCol == currentGoal.getCol())) {
                    heu = 80;
                }
            }
            
            // put the goal tile in row 2 thats not in the goal column
            // heu 60
            if (safeFromRow == 2 && safeToCol != currentGoal.getCol()) {
                heu = 50;
            }
            
            if ((safeFromRow == tileLoc[0] && safeFromCol == tileLoc[1]) && (safeToRow == 2 && safeToCol != currentGoal.getCol())) {
                heu = 70;
            }
        }
    } else if (getVal(currentGoal.getRow(), currentGoal.getCol()) != 0) {
        // make goal position available
        
        // a move out of the goal column an not on top of goal tile
        // heu 60
        if (safeFromCol == currentGoal.getCol() && safeToCol != tileLoc[1]) {
            heu = 60;
        }
        
        // a move out of the goal column
        // heu 50
        if (safeFromCol == currentGoal.getCol()) {
            heu = 50;
        }
    } else if (currentGoal.getRow() != 0 && getVal(currentGoal.getRow()-1, currentGoal.getCol()) == 0) {
        
        // An action to build up the to the goal position has hue of 55;
        if ((safeFromRow != tileLoc[0] && safeFromCol != tileLoc[1]) && (safeToRow < currentGoal.getRow() && safeToCol == currentGoal.getCol())) {
            heu = 55;
        }
    } else {
        // Put goal board in goal position
        
        // a move from the the goal tile location to the goal position
        // heu 100;
        
        if ((safeFromRow == tileLoc[0] && safeFromCol == tileLoc[1]) && (safeToRow == currentGoal.getRow() && safeToCol == currentGoal.getCol())) {
            heu = 100;
        }
    }
    
    return heu;
}

#endif /* LegalAction_h */
