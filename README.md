# Heuristic Mini SHRDLU

This project explores a C++ implementation of vectors, lists, stacks, queues, priority queues and heuristic algorithms to allow machine intelligence to solve Mini-SHRDLU, a game created by Demis Hassabis, CEO of Google DeepMind.

## Getting Started


### Prerequisites

Ensure you local environment has the `g++` compiler installed

### Clone the repositor, compile and run
The Mini-SHRDLU game can be played by cloning this repository 

```
git clone git@gitlab.com:wwdman/heuristic-mini-shrdlu.git
cd heuristic-mini-shrdlu
```

Then compiling and runing the `main` file
```
g++ main.cpp -o main.out
./main.out
```

This will present you wit the menu to select 3 different coal types

### Built With

* [C++](https://devdocs.io/cpp/)

## Mini-SHRDLU Description
SHRDLU is a name of software written in late 1960s by Terry Winograd at the MIT 
Artificial Intelligence (AI) Laboratory. The program was used for demonstrating 
how AI technologies could be used in natural language processing1. Inspired by but
different from SHRDLU, Mini-SHRDLU is a computer game designed by Demis Hassabis, 
the CEO of Google DeepMind, and his research team for testing their AI algorithms.

A Mini-SHRDLU game contains `k` numbered blocks (from `1 to k` respectively) on an
`n*n` vertically suspended board (`k ≤ (n*n)-n`). All blocks have to be placed in the board,
distributed in the columns, next each other down to the bottom in each column. A
block on the top of a column can be moved from one column and deposited on the top
of another column if space is available. At the beginning of each game, the board is
randomly configured, called the initial state. A player of the game is then given a goal
or a set of goals to achieve by moving the blocks in sequence from the initial state. A
goal can be any properties of the positions of blocks.

### States
The State class represents any block configuration on a game board. The simplest 
way to represent a state of this game is a matrix of integers, where zero stands 
for blank and integers for block labels. A graphical example of a state is below
```
                            | 0 | 0 | 6 |
                            | 0 | 1 | 4 |
                            | 5 | 3 | 2 |
```

### Actions
The action class represents moving a block from one column to another column 
within a state (@see: State.h). Pair of integers are used (source, destination), 
to represent an action. For instance, (2,1) means moving the highest verticle 
block from Column 2 to Column 1. An action is considered legal in a state only 
if the destination column contains empty slots. In the below example there are 
the following legal actions: (0,1), (1, 0), (2, 0) and (2,1).
```
                            | 0 | 0 | 6 |
                            | 0 | 1 | 4 |
                            | 5 | 3 | 2 |
```
Once a legal action is applied to a state, the state will transition to its next 
state. For instance, if we apply the action (2,1) to the above-shown state, its 
next state will be:
```
                            | 0 | 6 | 0 |
                            | 0 | 1 | 4 |
                            | 5 | 3 | 2 |
```

### Goals
A goal can be any property of block configuration you want a game terminates with.
For instance, you can require Block 1 to be located at (0,0). In this case, the goal can
be represented as (1, 0, 0), where the first component stands for the label of the block
and the other two components represent the location coordinates. A goal state is any
state that satisfies the goal. Normally a goal can have several goal states.

#### Atomic Goals
A goal in the form (block, row, column) is called an atom goal, which means that the
block must be located at location (row, column). Atom goals can be further combined
in two different ways: disjunctive and conjunctive. 

#### Disjunctive Goals
If a set of atom goals {g1, g2, …, gm} are disjunctive, any state that satisfies 
any of the atom goals is a goal state. For instance, the disjunctive goal 
{(1, 0, 0), (1, 1, 0), (1, 2, 0)} means Block 1 must be in Column 0 no matter 
which row it is. 

#### Conjunctive Goals
If a set of atom goals {g1, g2, …, gm} are conjunctive, a goal state must satisfy 
all the atom goals in the set. For instance, the conjunctive goal 
{(1, 0, 0), (2, 0, 1), (3, 0, 2)} means Blocks 1, 2 & 3 must be all in the bottom 
row with the respective order. There are also other ways to represent a goal. If
we use ‘b’, ‘a’, ‘l’ and ‘r’ to denote ‘below’, ‘above’, ‘left of’ and ‘right of’
respectively, ‘6b2’ means Block 6 must be below Block 2 and ‘4l1’ means Block 4
must be at the left of Block 1. We all these goals are neighbourhood goals. Obviously
neighbourhood goals can be transferred into combinations of disjunctive and
conjunctive goals.

### Plans
A plan is a sequence of actions that can bring a game from the initial state to a goal
state. The task of this assignment is to design an algorithm that can automatically
generate a plan to execute from any given initial state and any given goals if the goals
are achievable. The typical way to find a plan is search: start from the initial state,
choose one of the legal actions to execute to get into the next state; if a goal state is
reached, stop; otherwise, choose another action to execute; keep searching until a
goal state is reached. 

## Authors

* **Daniel Stratti** 


## Acknowledgments

* README.md Template: https://gist.github.com/PurpleBooth/109311bb0361f32d87a2

