//
//  Goal.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti
// 18577857

#ifndef Goal_h
#define Goal_h

#include <vector>
#include <iostream>

class Goal {
private:
    int numberToMove;
    int row;
    int col;
    
public:
    Goal(){}
    /* Defaul;t null constructor
     */
    
    Goal(int num, int toRow, int toCol):numberToMove(num), row(toRow), col(toCol){}
    /* Initializes the goal with its tile value, and destination row and column
     *
     * @param num - the number that has to be in the goal position
     * @param row - the row of the goal position
     * @param col - the column of the goal position.
     */
    
    //Goal(Goal &copy):numberToMove(copy.getNumber()), row(copy.getRow()), col(copy.getCol()){}
    /* Initializes the goal from another goal with its tile value, and destination row and column
     *
     * @param copy - the goal to be copied
     */
    
    int getNumber() const;
    /* returns the value of the number to be moved
     *
     */
    
    int getRow() const;
    /* returns the value of the row of the goal position
     *
     */
    
    int getCol() const;
    /* returns the value of the column of the goal position
     *
     */
    
    void print();
    /* Prints the goal to screen
     * (num, row, col)
     */
    
    bool operator<(const Goal gToCompare) const;
    /* Overloads the less than operator (<)
     */
    
    bool operator==(Goal &check) const;
    /* Overloads the Equals too operator
     *
     */
    
    void operator=(const Goal &check);
    /* Overloads the assignment operator
     *
     */
};

/* Get NUmber */
int Goal::getNumber() const{
    return this->numberToMove;
}

/* Get row */
int Goal::getRow() const{
    return this->row;
}

/* Get column */
int Goal::getCol() const{
    return this->col;
}

/* Print */
void Goal::print() {
    cout << "(" << getNumber() << ", " << getRow() << ", " << getCol() << ")" << endl;
}

/* Less than Overload */
bool Goal::operator<(const Goal gToCompare) const {
    if (this->row < gToCompare.row) {
        return false;
    } else if (this->row == gToCompare.row) {
        if (this->col > gToCompare.col) {
            return true;
        }
    }else {
        return true;
    }
    return false;
}

/* Equals to overload */
bool Goal::operator==(Goal &check) const {
    if (this->numberToMove == check.getNumber() && this->row == check.getRow() && this->col == check.getCol()) {
        return true;
    }
    
    return false;
}

/* Assignment overload */
void Goal::operator=(const Goal &check) {
    this->numberToMove = check.getNumber();
    this->row = check.getRow();
    this->col = check.getCol();
}
#endif /* Goal_h */
