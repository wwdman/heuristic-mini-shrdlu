//
//  Plan.h
//  DSA Ass 01 v 10.1
//
//  Created by Daniel Stratti on 6/4/17.
//  Copyright © 2017 Daniel Stratti. All rights reserved.
//
// Daniel Stratti
// 18577857

#ifndef Plan_h
#define Plan_h

#include "legalAction.h"
#include "Goal.h"
#include "Disjunctive.h"
#include "Conjunctive.h"

#include <queue>
#include <vector>
#include <stack>

using namespace std;

//const bool DEBUG_MODE = true;

class Plan {
private:
    State currentS;
    State initialS;
    stack<Action> movesMade;
    queue<LegalAction> levels;
    Goal goalState;
    vector<State> visitedState;
    
public:
    Plan(State curr, Goal goal);
    /* Constructor to Initialize a Plan for
     * a Atom Goal
     *
     * @param curr - the Initail state
     * @param goal - The Atom Goal to be Achieved
     */
    
    Plan(State curr, Disjunctive goal);
    /* Constructor to Initialize a Plan for
     * a Disjunctive set of goals
     *
     * @param curr - the Initail state
     * @param goal - The Disjunctive object to be Achieved
     */
    
    Plan(State curr, Conjunct goal);
    /* Constructor to Initialize a Plan for
     * a Conjunctive goal
     *
     * @param curr - the Initail state
     * @param goal - The Conjunctive object to be Achieved
     */
    
    void takeMove();
    /* Takes the action with the highest heuristic value
     * and keeps track of the states visited, it ensures no 
     * state is visited twice.
     */
    
    bool checkState(State toCheck, Goal curGoal);
    /* Checks if the goal state exists in the parsed state
     *
     * @param toCheck - the state to check if the goal existis within it
     * @param curGoal - the goal state to check for
     * @return bool - true if goal state is in goal position | 
                      false if goal state is not in goal position
     */
    
    bool searchForState(Goal goalS);
    /* Search through possible actions
     * to find the goal ste from the current state
     *
     * @param goalS - the current Goal to find
     * @return true if goal state found | false if goal stae not found
     * or 100 steps taken
     */
    
    bool searchVisited(State &currState);
    /* Checks to se if the state resulting from an action
     * has been visited before
     *
     * @param currState - the stae that would resul from a certain action taking place.
     * @return true if found | fals if not
     */
    
};

/* Constructo Atom Goal */
Plan::Plan(State curr, Goal goal):currentS(curr), initialS(curr), goalState(goal){
    visitedState.push_back(curr);
    
    if (searchForState(goal)) {
        cout << "Goal was achieved" << endl;
    } else {
        cout << "GOAL WAS NOT ACHIEVED" << endl;
    }
}

/* Constructor Disjunctive Goal */
Plan::Plan(State curr, Disjunctive goal):currentS(curr), initialS(curr){
    bool found = false;
    
    // Attempt goals until one is achieved or all are failed
    while (!found && goal.getDisGoal()->size() > 0) {
        
        visitedState.push_back(initialS);
        
        // Print the goal being attempted
        goal.getDisGoal()->back().print();
        
        // Set goal state to current goal and set the current state to the initail state
        goalState = goal.getDisGoal()->back();
        currentS = initialS;
        
        // Search for goal state
        found = searchForState(goal.getDisGoal()->back());
        
        // Pop failed goal off the goal list
        if (!found) {
            goal.getDisGoal()->pop_back();
        }
        
        // reset visited states
        while (!visitedState.empty()) {
            visitedState.pop_back();
        }
    }
    
    
    if (found) {
        goal.getDisGoal()->back().print();
        cout << "THE ABOVE GOAL WAS ACHIEVERD" << endl;
        currentS.print();
    } else {
        cout << "NO GOALS WERE ACHIEVED" << endl;
    }
}

/* Constructor Conjunctive Goal */
Plan::Plan(State curr, Conjunct goal):currentS(curr), initialS(curr) {
    bool found = true;
    visitedState.push_back(curr);
    
    // try all goals
    while(!goal.getConGoal()->empty()) {
        
        // Get the goal from the priority queue (the goal with lowest row val).
        //  set it as teh current goal state, pop it out of the queue
        Goal goalToTry = goal.getConGoal()->top();
        goalState = goalToTry;
        goal.getConGoal()->pop();
        
        // Print the goal being attempted
        goalToTry.print();
        
        // Search for goal
        found = searchForState(goalToTry);
        
        // If failed hop out of loop
        if (!found) {
            break;
        }
    }
    
    if(!found) {
        cout << "Sorry I could not achieve your conjunctive goal" << endl;
    } else {
        cout << "GOALS ACHIEVED" << endl;
        currentS.print();
    }
    
}

/* Take Move */
void Plan::takeMove() {
    LegalAction currentChoice(currentS, goalState);
    
    // Get top move
    Action moveToTake = currentChoice.getLegalMoves()->top();
    currentChoice.getLegalMoves()->pop();
    
    // Get resulting state from action
    Action debug = moveToTake;
    State resultState = currentChoice.move(currentChoice.copy(), debug);
    
    // change actions if state has been visited
    while (searchVisited(resultState) && !currentChoice.getLegalMoves()->empty()) {
        
        moveToTake = currentChoice.getLegalMoves()->top();
        currentChoice.getLegalMoves()->pop();
        
        resultState = currentChoice.move(currentChoice.copy(), moveToTake);
    }
    
    // print the chosen action
    cout << "--- Choice ---" << endl;
    cout << "--- move to take" << endl;
    moveToTake.print();
    cout << "move made" << endl;
    
    // Add it to moves made and levels
    movesMade.push(moveToTake);
    levels.push(currentChoice);
    
    // Make move
    visitedState.push_back(resultState);
    currentS = resultState;
    
    // Print the resulting state
    currentS.print();
    
}

/* Search For State */
bool Plan::searchForState(Goal goalS) {
    int steps = 0;
    
    // Atempt to find the goal within 100 steps
    while (!checkState(currentS, goalS) && steps < 100) {
        steps++;
        cout << endl;
        cout << "\t ------ step " << steps << " ------"<< endl;
        takeMove();
    }
    
    return checkState(currentS, goalS);
    
}

/* Check State */
bool Plan::checkState(State toCheck, Goal curGoal) {
    
    // Check if goal is achieved
    if (toCheck.getVal(curGoal.getRow(), curGoal.getCol()) == curGoal.getNumber()) {
        return true;
    }
    return false;
}

/* Search visited states */
bool Plan::searchVisited(State &currState) {
    
    // Check if intended state has been visited
    for (State vState : visitedState) {
        if (vState == currState) {
            return true;
        }
    }
    return false;
}
#endif /* Plan_h */
